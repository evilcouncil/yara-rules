/*
Detect an ip address
*/

rule ip_addr {
meta:
    author = "Jon Heise"
strings:
    $ipv4 = /([0-9]{1,3}\.){3}[0-9]{1,3}/ wide ascii
    $ipv4_2 = /([0-9]{1,3}\.){3}[0-9]{1,3}/ ascii
condition:
    any of them
}
