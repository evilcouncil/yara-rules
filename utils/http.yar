/*
Detect file linked via http
*/

rule HTTP_LINK {
meta:
    author = "Jon Heise"
strings:
    $http = /http\:\/\//
    $https = /https\:\/\//
condition:
    any of them
}
